# Searchs for "Taekwondo" over past 16 years

Taekwondo has had consistent searches, ranging around 25-50. However, in
August of 2008, Taekwondo had a search spike of 100.  We later realized this
was because Taekwondo was one of the sports that featured during the 2008
Summer Olympics.  Also, there has been a spike of searches every 4 years
around August, and this is most likely due to the Summer Olympics.

- Item 1
- Item 2
