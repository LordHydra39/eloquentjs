# Metadata of Birds

Unfortunately, there was no metadata for Google Trends so we had permission to get metadata from code.org. The metadata includes id, scientific name, conservation status, primary color, diet, and image of range.
