const arts = ["make a paper snowflake", "glitter glue", "painting", "color a coloring book", "watch a youtube video on how to draw feet"]
const outdoors = ["go hiking", "go swimming", "play a sport", "walk the dog", "go biking"]
const technology = ["play a videogame", "read an online book", "listen to music", "go on social media", "text your friends"];
const readline = require('readline');
 
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
 
function ask(prompt) {
  return new Promise((resolve, reject) => {
    rl.question(prompt, input => resolve(input));
  });
}
 
async function main() {
  let name = await ask("\nWhat is your name? ");
  var activity = "A";
  let cat = await ask("\nSo, "+name+", what do you want to do to cure your boredom? (Type A for arts, O for outdoors, and T for technology)");
  if (cat == "A"){
    activity = arts[Math.floor((Math.random()*6)-1)];
  }
  if (cat == "O"){
    activity = outdoors[Math.floor((Math.random()*6)-1)];
  }
  if (cat == "T"){
    activity = technology[Math.floor((Math.random()*6)-1)];
  }
  console.log("You should "+activity+"!");
  process.exit();
}
 
main();

